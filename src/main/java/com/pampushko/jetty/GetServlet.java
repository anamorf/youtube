package com.pampushko.jetty;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Alexander Pampushko on 09.09.2016.
 */
public class GetServlet extends HttpServlet
{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		resp.setContentType("text/html; charset=utf-8");
		PrintWriter writer = resp.getWriter();

		String firstName = req.getParameter("firstname");

		writer.println("<!DOCTYPE html>");
		writer.println("<head>");
		writer.println("<meta charset=\"utf-8\">");
		writer.println("<title>Пример страницы на HTML5</title>");
		writer.println("</head>");

		writer.println("<body>");
		writer.println("<header>");
		writer.println("<hgroup>");
		writer.println("<h1>");
		writer.println("Hello " + firstName + "<br />");
		writer.println("Welcome to Servlets!");
		writer.println("</h1>");
		writer.println("</hgroup>");
		writer.println("</header>");
		writer.println("</body>");
		writer.println("</html>");
		writer.close(); //для завершения страницы закрыть поток

	}
}
