package com.pampushko.jetty.frontend;

import javax.servlet.ServletException;
import javax.servlet.SingleThreadModel;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Alexander Pampushko on 16.09.2016.
 */
public class LoginServlet extends HttpServlet implements SingleThreadModel
{
	@Override
	public String getServletInfo()
	{
		return super.getServletInfo();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		
		//если окей - то входим в систему
		
		//если не окей, то сообщаем что пароль неправильный
		
		//нам приходят логин и пароль
		String login = req.getParameter("login");
		String password = req.getParameter("password");
		
		//мы ищем пользователя по его логину
		User user = new UserManager().findUserByLogin(login);
		
		//если пользователь найден
		if (user != null)
		{
			if (user.getPassword().equals(password))
			{
				user.setLogged(true);
				req.getSession().setAttribute("user", user);
				resp.sendRedirect("/private/wellcome.jsp");
			}
			else
			{
				user.setLogged(false);
				req.getSession().setAttribute("user", user);
				resp.sendRedirect("/login.jsp");
			}
		}
		else //если пользователь не найден
		{
			//то нужно перенаправить его на страницу регистрации
			resp.sendRedirect("/register.jsp");
		}
		
		
	}
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		req.getRequestDispatcher("index.jsp").forward(req, resp);
	}
}
