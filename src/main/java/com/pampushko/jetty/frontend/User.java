package com.pampushko.jetty.frontend;

/**
 * Created by Alexander Pampushko on 17.09.2016.
 * Экземпляры класса, содержат информацию о пользователе
 * имя, пароль, залогинился пользователь или нет
 */
public class User
{
	//имя пользователя
	private String username;
	//пароль пользователя
	private String password;
	//признак того, что пользователь вошел в систему
	private boolean logged = false;

	
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}
	public String getPassword()
	{
		return password;
	}
	
	
	public void setLogged(boolean logged)
	{
		this.logged = logged;
	}
	
	public boolean isLogged()
	{
		return logged;
	}
}
