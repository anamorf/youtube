package com.pampushko.jetty;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Alexander Pampushko on 05.09.2016.
 */

@SuppressWarnings("serial")
@WebServlet(name = "myServlet")
public class HelloHttpServlet extends HttpServlet
{
	@Override
	protected void doGet( HttpServletRequest request,
	                      HttpServletResponse response ) throws ServletException,
			IOException
	{
		response.setContentType("text/html");
		response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().println("<h1>Hello from HelloServlet!</h1>");
	}
}